var ctx = document.getElementById('sentiment').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: sentiment_data['topic'],
            datasets: [{
                label: 'Polarity',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: sentiment_data['sentiment']
            },
            {
                label: 'Proportion',
                backgroundColor: 'rgb(72,209,204)',
                borderColor: 'rgb(72,209,204)',
                data: sentiment_data['sentiment_counts']
            },
        ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Sentiment by topic'
            }
        }
    });

    var ctx = document.getElementById('test').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: entity_data['topic'],
            datasets: [{
                label: 'Document Count',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: entity_data['count']
            }
        ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Named Entity Recognition'
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min:0
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('predictions').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: predictions_data['classes'],
            datasets: [{
                label: 'Predictions',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: predictions_data['confidences']
            }
        ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Predictions % Confidence'
            },
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        min:0,
                        max:100
                    }
                }]
            }
        }
    });

    var ctx = document.getElementById('earnings').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: earnings_data['classes'],
            datasets: [{
                label: 'Predictions',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: earnings_data['confidences']
            }
        ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Predictions % Confidence'
            },
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        min:0,
                        max:100
                    }
                }]
            }
        }
    });