from flask import Flask, request, render_template
from Database import DBClient
import logging
import os


app = Flask(__name__)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_configured_value(key: str, default: str = None) -> str:
    '''
    '''
    configured = os.getenv(key)
    if configured:
        return configured
    if default:
        return default
    raise Exception('no ' + key)


db_name = get_configured_value('DB_NAME')
db_port = get_configured_value('DB_PORT')
db_username = get_configured_value('DB_USERNAME')
db_password = get_configured_value('DB_PASSWORD')
db_url = get_configured_value('DB_URL')
db_client = DBClient(
    'postgresql://' + db_username +
    ':' + db_password +
    '@' + db_url + ':' + db_port +
    '/' + db_name
    )


def word_count(text: list) -> int:
    count = 0
    for item in text:
        count += len(item['title'].split(" "))
        count += len(item['body'].split(" "))
    return count


def section_count(text: list) -> int:
    return len(text)


def avg_section_length(text: list) -> int:
    counts = []
    for item in text:
        count = 0
        count += len(item['title'].split(" "))
        count += len(item['body'].split(" "))
        counts.append(count)
    return int(sum(counts)/len(counts))


def default_json_data() -> dict:
    return {
            'current_name': 'current_name',
            'current_year': 2020,
            'current_quarter': '2',
            'sentiment': [1, -0.5, .5, -.035],
            'sentiment_counts': [1, 1, 1, 1],
            'entities': [1, 2, 3, 4, 5, 6, 7],
            'entity_labels': ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            'text': [
                {'title': 'title', 'body': 'body'}],
            'stat_1': 1,
            'stat_2': 1,
            'stat_3': 1,
            'predictions': [100, 0, 0],
            'earnings': [100, 0],
            'companies': db_client.get_names(),
            'years': db_client.get_years()
            }


@app.route('/')
def home():
    logging.info("home route.")
    logging.info(db_client.get_years())
    data = {
        'companies': db_client.get_names(),
        'years': db_client.get_years()
        }
    return render_template('home.html', data=data, static_url_path='static/')


@app.route('/detail')
def detail():

    logging.info("detail route.")
    logging.info(request.args.get('companySelect'))
    logging.info(request.args.get('yearSelect'))
    logging.info(request.args.get('quarterSelect'))

    record = db_client.get_record(
        request.args.get('companySelect'),
        request.args.get('yearSelect'),
        request.args.get('quarterSelect')
        )

    logging.info(record)

    if (record is None):
        data = {}
        data['companies'] = db_client.get_names()
        data['years'] = db_client.get_years()
        return render_template(
            'not_found.html',
            data=data,
            static_url_path='static/'
            )
    else:
        data = record.get_data()
        data['companies'] = db_client.get_names()
        data['years'] = db_client.get_years()
        data['current_year'] = request.args.get('yearSelect')
        data['stat_1'] = word_count(data['text'])
        data['stat_2'] = section_count(data['text'])
        data['stat_3'] = avg_section_length(data['text'])

    return render_template('layout.html', data=data, static_url_path='static/')


if __name__ == "__main__":
    app.debug = True
    app.run(host='')
