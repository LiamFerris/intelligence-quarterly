from Database import Report

sample_report = Report()
sample_report.data = '{"test":"exists"}'


def test_dummy():
    assert(True)


def test_report():
    assert(type(sample_report) == Report)


def test_report_initialisation():
    assert(type(Report()) == Report)


def test_report_get_data_type():
    assert(type(sample_report.get_data()) == dict)


def test_report_get_data():
    assert(sample_report.get_data()['test'] == 'exists')
