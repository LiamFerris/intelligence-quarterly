import json
from sqlalchemy import Column, INT, VARCHAR, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exc
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


Base = declarative_base()


class Report(Base):
    '''
    Represents a DB row
    '''

    __tablename__ = 'reports'

    name = Column(VARCHAR, primary_key=True)
    year = Column(INT, primary_key=True)
    quarter = Column(INT, primary_key=True)
    data = Column(VARCHAR)

    def get_data(self) -> dict:
        '''
        Decodes and returns JSON.
        '''
        return json.loads(self.data)


class DBClient():

    def __init__(self, endpoint):
        '''
        '''
        self.connect_to_database(endpoint)

    def connect_to_database(self, endpoint: str):
        '''
        Connect to the DB
        '''
        try:
            engine = create_engine(endpoint)
            Base.metadata.bind = engine
            DBSession = sessionmaker(bind=engine)
            self.database_session = DBSession()
            self.database_engine = engine
            self.database_session.execute('SELECT 1')
        except exc.SQLAlchemyError as e:
            logging.info(repr(e))
            raise

    def get_record(self, name: str, year: str, qtr: str) -> Report:
        '''
        Insert into the DB.
        '''
        if (name is None):
            return None
        if (year is None):
            return None
        if (qtr is None):
            return None
        try:
            return self.database_session.query(Report).filter(
                Report.name == name).filter(
                    Report.year == int(year)).filter(
                        Report.quarter == int(qtr)).one()
        except exc.SQLAlchemyError as e:
            self.database_session.rollback()
            logging.error(e)

    def get_years(self) -> list:
        '''
        Insert into the DB.
        '''
        try:
            out = []
            for val in self.database_session.query(Report.year).distinct():
                out.append(int(str(val)[1:5]))
            return out
        except exc.SQLAlchemyError as e:
            self.database_session.rollback()
            logging.error(e)

    def get_names(self) -> list:
        '''
        Insert into the DB.
        '''
        try:
            out = []
            for val in self.database_session.query(
                    Report.name).distinct().order_by(Report.name):
                out.append(str(val)[2:-3])
            return out
        except exc.SQLAlchemyError as e:
            self.database_session.rollback()
            logging.error(e)
