import glob
import edgar
import pandas as pd
import requests
import logging
from bs4 import BeautifulSoup


class SECClient():
    '''
    This class is a wrapper around the SEC Edgar interface.

    Exposes the data as a pd.DataFrame.

    Expects a starting year to download data from and
    a local directory to store downloaded files to disk.
    '''

    def __init__(self, year_from: str = 2020, download_dir: str = 'files/'):
        '''
        SEC client constructor.
        '''
        logging.info("construction client...")
        self.base_url = 'https://www.sec.gov/Archives/'
        self.year_from = year_from
        self.download_dir = download_dir
        self.download_all_quareterlys()
        self.df = self.create_dataframe()

    def get_filenames(self) -> list:
        '''
        Returns all files in download directory.
        '''
        logging.info("getting filenames...")
        return glob.glob(self.download_dir + "*.tsv")

    def download_all_quareterlys(self):
        '''
        Download quarterly files locally.
        '''
        logging.info("downloading from edgar...")
        edgar.download_index(
            self.download_dir,
            self.year_from,
            skip_all_present_except_last=True)

    def create_dataframe(self) -> pd.DataFrame:
        '''
        '''
        logging.info("populating dataframe...")
        df = self.get_quarterlys_as_df()
        df = self.apply_doc_id_to_df(df)
        df = self.apply_doc_url_to_df(df)
        return df

    def get_quarterlys_as_df(self) -> pd.DataFrame:
        '''
        Get all quarterly files from local storage.
        Return a pandas DF.
        '''
        logging.info("constructing initial dataframe...")
        dfs = []
        for filename in self.get_filenames():
            dfs.append(pd.read_csv(
                filename,
                sep="|",
                header=None,
                names=[
                    'Central Index Key',
                    'Name',
                    'Filing',
                    'Date',
                    'Filename',
                    'URL']
                ))
            df = pd.concat(dfs, ignore_index=True)
        df = df[df['Filing'].isin(['10-K', '10-Q'])]
        return df

    def apply_doc_id_to_df(self, df: pd.DataFrame) -> pd.DataFrame:
        '''
        Add document ids to dataframe.
        '''
        logging.info("applying document ids to df...")
        doc_ids = []
        for row in df['URL'].tolist():
            doc_ids.append(self.get_doc_id_from_url(row))
        df['doc_id'] = doc_ids
        return df

    def apply_doc_url_to_df(self, df: pd.DataFrame) -> pd.DataFrame:
        '''
        Add document urls to dataframe.
        '''
        logging.info("applying document urls to dataframe...")
        doc_urls = []
        for i in range(0, len(df)):
            doc_urls.append(
                self.construct_doc_url_from_doc_id(
                    df.iloc[i]['URL'],
                    df.iloc[i]['doc_id']
                    )
                )
        df['doc_url'] = doc_urls
        return df

    def construct_doc_url_from_doc_id(self, url: str, doc_id: str) -> str:
        '''
        Construct the url for the document.
        '''
        url = url.replace("-index.html", "").replace("-", "")
        return self.base_url + url + "/" + doc_id

    def get_doc_id_from_url(self, url: str) -> str:
        '''
        Parse the htm page for the document id.
        '''
        response = requests.get(self.base_url + url)
        soup = BeautifulSoup(response.content, features='lxml')
        f = soup.find_all("td")[2].find("a").contents[0]
        return f
