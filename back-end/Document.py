import re
import urllib3
import html2text

MIN_SECTION_SIZE: int = 1000


class Section():

    def __init__(self, title: str, body: str):
        self.title = title
        self.body = body


class Document():

    def __init__(self, doc_url: str):
        http = urllib3.PoolManager()
        response = http.request('GET', doc_url)
        raw_document_text = html2text.html2text(str(response.data))
        raw_document_text = raw_document_text.replace("\\n", " ")
        raw_document_text = raw_document_text.replace("\n", " ")
        raw_document_text = raw_document_text.replace("|", " ")
        raw_document_text = raw_document_text.replace("\\r", " ")
        raw_document_text = raw_document_text.replace("\r", " ")
        raw_document_text = raw_document_text.replace("\\t", " ")
        raw_document_text = raw_document_text.replace("\t", " ")
        raw_document_text = raw_document_text.replace(",", " ")
        raw_document_text = raw_document_text.replace("(", " ")
        raw_document_text = raw_document_text.replace(")", " ")
        raw_document_text = raw_document_text.replace(".", " ")
        raw_document_text = raw_document_text.replace("-", " ")
        raw_document_text = raw_document_text.replace("_", " ")
        raw_document_text = raw_document_text.replace("\"", " ")
        raw_document_text = raw_document_text.replace("'", " ")
        raw_document_text = raw_document_text.replace("\\", " ")
        raw_document_text = raw_document_text.replace("\\\\", " ")
        raw_document_text = re.sub(r'[^\x00-\x7F]+', ' ', raw_document_text)
        raw_document_text = raw_document_text.lower()
        self.text = self.clean_text(raw_document_text)
        self.sections = self.section(self.text)

    def to_list(self) -> list:
        '''
        '''
        out = []
        for section in self.sections:
            out.append({'title': section.title, 'body': section.body})
        return out

    @staticmethod
    def clean_text(text: str) -> str:
        out = ''
        for item in text.split(' ')[1:]:
            not_digit = item.isdigit() is False
            has_size = len(item) > 0
            has_letters = re.search('[a-zA-Z0-9]', item)
            if (not_digit and has_size and has_letters):
                out += item + ' '
        return out

    @staticmethod
    def section(text: str) -> list:
        sections = []
        for i, item in enumerate(text.split('**')):
            if (len(item) >= MIN_SECTION_SIZE):
                if (i == 0):
                    title = "Report"
                else:
                    title = text.split('**')[i-1]
                if(sum(c.isalpha() for c in title)):
                    sections.append(Section(title, item))
        return sections
