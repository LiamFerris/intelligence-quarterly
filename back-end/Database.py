import json
from sqlalchemy import Column, INT, VARCHAR, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exc
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


Base = declarative_base()


class Report(Base):
    '''
    Represents a DB row
    '''

    __tablename__ = 'reports'

    name = Column(VARCHAR, primary_key=True)
    year = Column(INT, primary_key=True)
    quarter = Column(INT, primary_key=True)
    data = Column(VARCHAR)

    def __init__(self, name, year, quarter, data):
        self.name = name
        self.year = year
        self.quarter = quarter
        self.data = data

    def get_data(self) -> dict:
        '''
        Decodes and returns JSON.
        '''
        return json.loads(self.data)


class DBClient():

    def __init__(self, endpoint):
        '''
        '''
        self.connect_to_database(endpoint)

    def connect_to_database(self, endpoint: str):
        '''
        Connect to the DB
        '''
        try:
            engine = create_engine(endpoint)
            Base.metadata.bind = engine
            DBSession = sessionmaker(bind=engine)
            self.database_session = DBSession()
            self.database_engine = engine
            self.database_session.execute('SELECT 1')
        except exc.SQLAlchemyError as e:
            logging.info(repr(e))
            raise

    def create_records(self, records: Report):
        '''
        Insert into the DB.
        '''
        try:
            self.database_session.add(records)
            self.database_session.commit()
        except exc.SQLAlchemyError as e:
            self.database_session.rollback()
            logging.error(e)
