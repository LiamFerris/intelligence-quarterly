from Document import Document
from Database import Report

sample_report = Report('name', 2000, 4, '{"test":"exists"}')

url = 'https://www.sec.gov/'
s = 'Archives/edgar/data/1000045/000119312519039489/d676198d10q.htm'
doc = Document(url + s)


def test_dummy():
    assert(True)


def test_document_init():
    assert(type(doc) == Document)


def test_document_sections_init():
    assert(type(doc.sections) == list)


def test_document_to_list():
    assert(type(doc.to_list()) == list)


def test_document_to_list_title():
    assert(type(doc.to_list()[0]['title']) == str)


def test_document_to_list_body():
    assert(type(doc.to_list()[0]['body']) == str)


def test_document_clean_text():
    assert(type(Document.clean_text("")) == str)


def test_document_section():
    assert(type(Document.section("")) == list)


def test_document_clean_digit_check():
    assert(Document.clean_text('2') == '')


def test_report():
    assert(type(sample_report) == Report)


def test_report_initialisation():
    assert(type(Report('name', 2000, 4, '{"test":"exists"}')) == Report)


def test_report_get_data_type():
    assert(type(sample_report.get_data()) == dict)


def test_report_get_data():
    assert(sample_report.get_data()['test'] == 'exists')


def test_report_name():
    assert(sample_report.name == 'name')


def test_report_year():
    assert(sample_report.year == 2000)


def test_report_quarter():
    assert(sample_report.quarter == 4)


def test_report_data():
    assert(sample_report.data == '{"test":"exists"}')
