from SECClient import SECClient
from Document import Document
from Database import DBClient, Report
import logging
import json
from sklearn.feature_extraction.text import CountVectorizer
from textblob import TextBlob
from statistics import mean
import joblib
import os
import re


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_configured_value(key: str, default: str = None) -> str:
    '''
    '''
    configured = os.getenv(key)
    if configured:
        return configured
    if default:
        return default
    raise Exception('no ' + key)


model_earnings = joblib.load('models/earning.joblib')
model_predictions = joblib.load('models/performance.joblib')


def get_predictions_probs(row):
    return get_pct(model_predictions, row)


def get_earnings_probs(row):
    return get_pct(model_earnings, row)


def get_pct(model, row):
    X = model['tfidf'].transform([re.sub(r'[^a-zA-Z ]+', '', row)])
    probs = model['clf'].predict_proba(X).tolist()[0]
    pct = []
    for num in probs:
        pct.append(num*100)
    return pct


def get_json_data(df_row, year, qtr, json_data, text):
    entity_labels, entities_scores = get_entities(text)
    return {
        'current_name': df_row['Name'],
        'current_year': 2020,
        'current_quarter': qtr,
        'sentiment': get_sentiment_scores(text),
        'sentiment_counts': get_sentiment_counts(text),
        'entities': entities_scores,
        'entity_labels': entity_labels,
        'text': json_data,
        'predictions': get_predictions_probs(text),
        'earnings': get_earnings_probs(text)
        }


def scan_vectorizer(vectorizer, key_words, feature_length):
    features = []
    for feature in vectorizer.get_feature_names():
        if (feature.split(" ")[feature_length] in key_words):
            features.append(feature)
    return features


def get_feature_sentiment(feature_array):
    sentiments = []
    for feature in feature_array:
        testimonial = TextBlob(feature)
        sentiments.append(testimonial.sentiment.polarity)
    if (sentiments == []):
        return 0
    return mean(sentiments)


def get_entities(text):
    try:
        vectorizer = CountVectorizer(
            ngram_range=(1, 1),
            strip_accents='unicode',
            stop_words='english',
            analyzer='word',
            max_features=10)
        X = vectorizer.fit_transform([text])
        entities = vectorizer.get_feature_names()
        enitity_labels = list(
            map(lambda x: round(float(x), 2), X.toarray()[0]))
        return entities, enitity_labels
    except ValueError:
        return ['not found'], [0]


def get_sentiment_scores(text):
    try:
        vectorizer = CountVectorizer(
            ngram_range=(15, 15),
            strip_accents='unicode',
            stop_words='english',
            analyzer='word')
        vectorizer.fit_transform([text])
        risk_features = scan_vectorizer(
            vectorizer, ['risk', 'risky'], 7)
        environmental_features = scan_vectorizer(
            vectorizer, ['carbon', 'oil', 'environment', 'renewables'], 7)
        debt_features = scan_vectorizer(
            vectorizer, ['debt', 'debtors', 'loan', 'loans'], 7)
        legal_features = scan_vectorizer(
            vectorizer, ['lawsuit', 'sue', 'legal', 'law', 'court'], 7)
        debt_sentiment = get_feature_sentiment(debt_features)
        environment_sentiment = get_feature_sentiment(environmental_features)
        risk_sentiment = get_feature_sentiment(risk_features)
        legal_sentiment = get_feature_sentiment(legal_features)
        return [
            round(debt_sentiment, 2),
            round(environment_sentiment, 2),
            round(risk_sentiment, 2),
            round(legal_sentiment, 2)
            ]
    except ValueError:
        return [0, 0, 0, 0]


def get_sentiment_counts(text):
    try:
        vectorizer = CountVectorizer(
            ngram_range=(15, 15),
            strip_accents='unicode',
            stop_words='english',
            analyzer='word')
        vectorizer.fit_transform([text])
        risk_features = scan_vectorizer(
            vectorizer, ['risk', 'risky'], 7)
        environmental_features = scan_vectorizer(
            vectorizer, ['carbon', 'oil', 'environment', 'renewables'], 7)
        debt_features = scan_vectorizer(
            vectorizer, ['debt', 'debtors', 'loan', 'loans'], 7)
        legal_features = scan_vectorizer(
            vectorizer, ['lawsuit', 'sue', 'legal', 'law', 'court'], 7)
        m = max(
            len(risk_features),
            len(environmental_features),
            len(debt_features),
            len(legal_features)
            )
        if (m == 0):
            return [0, 0, 0, 0]
        return [
            round((len(risk_features)) / m, 2),
            round((len(environmental_features)) / m, 2),
            round((len(debt_features)) / m, 2),
            round((len(legal_features)) / m, 2),
            ]
    except ValueError:
        return [0, 0, 0, 0]


db_name = get_configured_value('DB_NAME')
db_port = get_configured_value('DB_PORT')
db_username = get_configured_value('DB_USERNAME')
db_password = get_configured_value('DB_PASSWORD')
db_url = get_configured_value('DB_URL')
db_client = DBClient(
    'postgresql://' + db_username +
    ':' + db_password +
    '@' + db_url + ':' + db_port +
    '/' + db_name
    )


logging.info("downloading SEC data...")
sec_client = SECClient(2017, 'back-end/files/')
logging.info("download complete...")
logging.info(sec_client.df.sample(1))

index = 0
while (index + 10 < len(sec_client.df)):
    df = sec_client.df[index:index+10]
    logging.info(len(sec_client.df))
    logging.info(index)

    docs = []
    logging.info("generating documents...")
    for row in df['doc_url'].tolist():
        docs.append(Document(row))

    logging.info("collecting text...")
    logging.info(len(df))
    logging.info(len(docs))

    texts = []
    for row in docs:
        out = ""
        for section in row.to_list():
            out += section['body'] + " "
        texts.append(out)

    logging.info(len(df))
    logging.info(len(texts))
    df['text'] = texts

    logging.info("writing to csv...")
    df.to_csv("output.csv", index=False)

    logging.info("writing to json...")

    json_data = {}
    json_data['data'] = []
    for doc in docs:
        json_data['data'].append(doc.to_list())

    with open('data.json', 'w') as f:
        json.dump(json_data, f)

    logging.info("writing to database...")
    for i in range(len(df)):
        name = df.iloc[i]['Name']
        date = str(df.iloc[i]['Date'])
        year = int(date.split('-')[0])
        m = int(date.split('-')[1])
        if m < 3:
            qtr = 1
        elif m < 6:
            qtr = 2
        elif m < 9:
            qtr = 3
        else:
            qtr = 4
        report = Report(
            name,
            year,
            qtr,
            json.dumps(get_json_data(
                df.iloc[i],
                year,
                qtr,
                json_data['data'][i],
                texts[i]
                )
            ))
        db_client.create_records(report)
    index += 10
